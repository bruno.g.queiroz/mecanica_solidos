/*
 * Author: Bruno Queiroz
 * Date: 17 junho 2017
 * Description:
 *      Calcula a tração a compressão e o momento fletor
 **/

$(function () {

$(".calculardor").click(function() {
  var obj = {L1:$("#L1").val(),
             L2:$("#L2").val(),
             L3:$("#L3").val(),
             L4:$("#L4").val(),
             L5:$("#L5").val(),
             L6:$("#L6").val()
           }
  var forca = $("#forcaApli").val();
  tracao(forca,area(obj));
  compressao(forca,area(obj));
  var objFletor = {
                    w: forca,
                    l: $("#L7").val(),
                    x: $("#XFletor").val()
                  }
  flexao(objFletor);
})

});

function area(obj) {
  var result = (parseInt(obj.L1) * parseInt(obj.L2)) + (parseInt(obj.L3) * parseInt(obj.L5)) + (parseInt(obj.L4) * parseInt(obj.L6));
  return result;
}

function tracao(forca, area) {
  var t = (parseInt(forca) / parseInt(area));
  $("#ResultTracao").text("Resultado da Tração: "+t);
}

function compressao(forca, area) {
  var c = (parseInt(forca) / parseInt(area));
  $("#ResultCompressao").text("Resultado da Compressão: "+c);
}

function flexao(obj) {
  var m = (obj.w/2)*((obj.l*obj.x) - (obj.x*obj.x));
  $("#ResultFlexao").text("Resultado da Flexão: "+m);
}
